import { CreateUserDto } from "./user/create-user.dto";
import { User } from "./user/user.model";

declare const Controller: any;
declare const Delete: any;
declare const Get: any;
declare const Patch: any;
declare const Post: any;
declare const Put: any;

@Controller("/user")
export class UserController {
  @Get("/list")
  getAllUsers(): User[] {
    return [
      {
        password: "",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ] as User[];
  }

  @Post("/new")
  createUser(user: CreateUserDto): User {
    return user as User;
  }

  @Put("/update")
  updateUser(user: User): User {
    return user;
  }

  @Delete("/delete")
  deleteUser(userId: User["id"]): boolean {
    console.log(userId);
    return true;
  }

  @Patch("/patch")
  patchUser(userId: User["id"], patch: Partial<User>) {
    if (Math.random() > 0.5) {
      return {
        id: userId,
        ok: true,
        message: "created",
        user: patch,
      } as const;
    } else {
      return {
        ok: false,
        error: "FAILUREEE",
        sqlError: 8080,
      } as const;
    }
  }
}
