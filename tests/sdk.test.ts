import path from "path";
import { sdk } from "../src/main";
import { NestReader } from "../src/reader/nestjs-reader";
import { AngularWriter } from "../src/writer/angular-writer";

sdk(
  path.join(__dirname, "test.controller.ts"),
  __dirname + "/output/test.service.ts",
  new NestReader(),
  new AngularWriter(),
  {
    outputJson: __dirname + "/output/test.service.json",
  }
);
