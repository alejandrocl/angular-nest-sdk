# Angular Nest SDK

Angular Nest SDK is a cli command that generates Angular services from NestJS controllers.

## Usage
```sh
npx @acharluk/ngsdk <input file> [output file]
```

## Example

Given this NestJS controller:

```ts
import { Controller, Delete, Get, Patch, Post, Put } from "@nestjs/common";
import { CreateUserDto } from "./user/create-user.dto";
import { User } from "./user/user.model";

@Controller("/user")
export class UserController {
  @Get("/list")
  getAllUsers(): User[] {
    return [
      {
        password: "",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ] as User[];
  }

  @Post("/new")
  createUser(user: CreateUserDto): User {
    return user as User;
  }

  @Put("/update")
  updateUser(user: User): User {
    return user;
  }

  @Delete("/delete")
  deleteUser(userId: User["id"]): boolean {
    console.log(userId);
    return true;
  }

  @Patch("/patch")
  patchUser(userId: User["id"], patch: Partial<User>) {
    if (Math.random() > 0.5) {
      return {
        id: userId,
        ok: true,
        message: "created",
        user: patch,
      } as const;
    } else {
      return {
        ok: false,
        error: "FAILUREEE",
        sqlError: 8080,
      } as const;
    }
  }
}
```

It generates this Angular service:

```ts
import { Injectable, Observable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CreateUserDto } from "./user/create-user.dto";
import { User } from "./user/user.model";

@Injectable({ providedIn: "root" })
export class UserController {
  constructor(private http: HttpClient) {}

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>("/user/list");
  }

  createUser(user: CreateUserDto): Observable<User> {
    return this.http.post<User>("/user/new", user);
  }

  updateUser(user: User): Observable<User> {
    return this.http.put<User>("/user/update", user);
  }

  deleteUser(userId: User["id"]): Observable<boolean> {
    return this.http.delete<boolean>("/user/delete", userId);
  }

  patchUser(
    userId: User["id"],
    patch: Partial<User>
  ): Observable<
    | {
        readonly id: string;
        readonly ok: true;
        readonly message: "created";
        readonly user: Partial<User>;
        readonly error?: undefined;
        readonly sqlError?: undefined;
      }
    | {
        readonly ok: false;
        readonly error: "FAILUREEE";
        readonly sqlError: 8080;
        readonly id?: undefined;
        readonly message?: undefined;
        readonly user?: undefined;
      }
  > {
    return this.http.patch<
      | {
          readonly id: string;
          readonly ok: true;
          readonly message: "created";
          readonly user: Partial<User>;
          readonly error?: undefined;
          readonly sqlError?: undefined;
        }
      | {
          readonly ok: false;
          readonly error: "FAILUREEE";
          readonly sqlError: 8080;
          readonly id?: undefined;
          readonly message?: undefined;
          readonly user?: undefined;
        }
    >("/user/patch", userId, patch);
  }
}
```