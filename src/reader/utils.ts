import ts from "typescript";

export type StartsWith<
  U extends string,
  T extends string
> = U extends `${T}${infer _}` ? U : never;

export type CallableWithArg<T, A> = {
  [K in keyof T]: T[K] extends (args: A) => any ? K : never;
}[keyof T];

export type TsIsNodeType = StartsWith<
  CallableWithArg<typeof ts, ts.Node>,
  "is"
>;

export const findNodeWithType = (
  node: ts.Node,
  is: TsIsNodeType[],
  isIndex: number = 0
): ts.Node | undefined => {
  return ts.forEachChild(node, (child) => {
    const iss = is[isIndex];

    if (iss && ts[iss] && ts[iss](child)) {
      isIndex++;

      if (is[isIndex]) {
        return findNodeWithType(child, is, isIndex);
      }

      return child;
    }

    return findNodeWithType(child, is);
  });
};
