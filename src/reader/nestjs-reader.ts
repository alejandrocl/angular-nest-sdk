import ts from "typescript";
import { ReaderAdapter } from "../common/reader-adapter";
import {
  HttpMethod,
  SdkImport,
  SdkMethod,
  SdkParameter,
  SdkTree,
} from "../common/sdk-tree";
import { findNodeWithType } from "./utils";

export class NestReader implements ReaderAdapter {
  private readonly EXCLUDED_IMPORTS = ["@nestjs/*"];

  read(program: ts.Program, fileName: string): SdkTree {
    const sourceFile = program.getSourceFile(fileName);
    const checker = program.getTypeChecker();

    if (!sourceFile) {
      throw new Error("NestReader::read -> sourceFile is undefined");
    }

    return {
      name: this.readName(sourceFile),
      path: this.readPath(sourceFile),
      imports: this.readImports(sourceFile),
      methods: this.readMethods(sourceFile, checker),
    };
  }

  private readName(sourceFile: ts.SourceFile): string {
    // TODO: Remove this as casting with better typing
    const classDecl = (
      findNodeWithType(sourceFile, [
        "isClassDeclaration",
      ]) as ts.ClassDeclaration
    )?.name
      ?.getText(sourceFile)
      .replace("Controller", "Service")
      .replace("controller", "service");

    if (!classDecl) {
      throw new Error("NestReader::readName -> Cannot extract class name");
    }

    return classDecl;
  }

  private readPath(sourceFile: ts.SourceFile): string {
    const decorators = findNodeWithType(sourceFile, [
      "isDecorator",
      "isCallExpression",
      "isStringLiteral",
    ]);
    const decoratorText = decorators?.getText(sourceFile).replace(/['"]/g, "");

    if (!decoratorText) {
      throw new Error("NestReader::readPath -> Cannot extract decorator");
    }

    return decoratorText;
  }

  private readImports(sourceFile: ts.SourceFile): SdkImport[] {
    const imports: SdkImport[] = [];

    ts.forEachChild(sourceFile, (node) => {
      if (ts.isImportDeclaration(node)) {
        const specifier = findNodeWithType(node, [
          "isImportClause",
          "isNamedImports",
          "isImportSpecifier",
        ])?.getText(sourceFile);

        const importPath = findNodeWithType(node, [
          "isImportClause",
          "isStringLiteral",
        ])
          ?.getText(sourceFile)
          .replace(/['"]/g, "");

        if (!specifier) {
          throw new Error(
            "NestReader::readImports -> importSpecifier undefined"
          );
        }

        if (!importPath) {
          throw new Error("NestReader::readImports -> importPath undefined");
        }

        if (
          !new RegExp(`(${this.EXCLUDED_IMPORTS.join("|")})`).test(importPath)
        ) {
          imports.push({
            classes: [specifier],
            path: importPath,
          });
        }
      }
    });

    return imports;
  }

  private readMethods(
    sourceFile: ts.SourceFile,
    checker: ts.TypeChecker
  ): SdkMethod[] {
    // TODO: Remove this as with better typing
    const controllerClass = findNodeWithType(sourceFile, [
      "isClassDeclaration",
    ]) as ts.ClassDeclaration;

    if (!controllerClass.members) {
      throw new Error("NestReader::readMethods -> error extracting methods");
    }

    return controllerClass.members
      .filter((member) => ts.isMethodDeclaration(member))
      .map((member) => {
        // TODO: Fix all this 'as' with better typing

        const functionName = member.name?.getText(sourceFile);

        const httpMethod = this.extractMethodDecorator(
          member as ts.MethodDeclaration,
          sourceFile
        );

        const endpoint = this.extractMethodEndpoint(
          member as ts.MethodDeclaration,
          sourceFile
        );

        const parameters: SdkParameter[] = (
          member as ts.MethodDeclaration
        ).parameters.map((p) => this.extractMethodParameterData(p, sourceFile));

        const signatureDecl = checker.getSignatureFromDeclaration(
          member as ts.MethodDeclaration
        );

        if (!signatureDecl) {
          throw new Error(
            "NestReader::readMethods -> error extracting return type"
          );
        }

        const returnType = checker.typeToString(
          signatureDecl.getReturnType(),
          undefined,
          ts.TypeFormatFlags.NoTruncation
        );

        return {
          functionName,
          httpMethod,
          endpoint,
          parameters,
          returnType,
        } as SdkMethod;
      });
  }

  private extractMethodDecorator(
    method: ts.MethodDeclaration,
    sourceFile: ts.SourceFile
  ): HttpMethod {
    const decoratorNode = findNodeWithType(method, ["isDecorator"]);
    const decoratorText = decoratorNode?.getText(sourceFile);

    if (!decoratorText) {
      throw new Error(
        "NestReader::extractMethodDecorator -> Error extracting decorator"
      );
    }

    const match = RegExp(/(Get|Post|Put|Delete|Patch|Options|Head)/).exec(
      decoratorText
    );

    if (!match) {
      throw new Error(
        "NestReader::extractMethodDecorator -> Error extracting http method"
      );
    }

    return match[0].toUpperCase() as HttpMethod;
  }

  private extractMethodEndpoint(
    method: ts.MethodDeclaration,
    sourceFile: ts.SourceFile
  ) {
    const endpoint = findNodeWithType(method, ["isStringLiteral"])
      ?.getText(sourceFile)
      .replace(/['"]/g, "");

    if (!endpoint) {
      throw new Error(
        "NestReader::extractMethodEndpoint -> Cannot extract endpoint"
      );
    }

    return endpoint;
  }

  private extractMethodParameterData(
    parameter: ts.ParameterDeclaration,
    sourceFile: ts.SourceFile
  ): SdkParameter {
    // TODO: Check for undefineds
    return {
      name: parameter.name.getText(sourceFile),
      type: parameter.type?.getText(sourceFile) ?? "unknown",
    };
  }
}
