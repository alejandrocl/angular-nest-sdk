#!/usr/bin/env node

import { exit } from "process";
import { sdk } from "../main";
import { NestReader } from "../reader/nestjs-reader";
import { AngularWriter } from "../writer/angular-writer";
import { existsSync } from "fs";

if (process.argv.length === 3) {
  const inputFile = process.argv[2];
  const outputFile = inputFile?.replace("controller", "service");

  if (!inputFile || !outputFile) {
    exit(1);
  }

  if (!existsSync(inputFile)) {
    console.error("File not found: " + inputFile);
    exit(1);
  }

  sdk(inputFile, outputFile, new NestReader(), new AngularWriter());
} else if (process.argv.length === 4) {
  const inputFile = process.argv[2];
  const outputFile = process.argv[3];

  if (!inputFile || !outputFile) {
    exit(1);
  }

  if (!existsSync(inputFile)) {
    console.error("File not found: " + inputFile);
    exit(1);
  }

  sdk(inputFile, outputFile, new NestReader(), new AngularWriter());
} else {
  console.log("Usage: ngsdk <input file> [output file]");
}
