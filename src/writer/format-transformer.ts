import ts from "typescript";

// TODO: Remove this any
export const AddNewLines = (context: any) => {
  const { factory } = context;
  const newLine = factory.createIdentifier("\n");
  const visit: ts.Visitor = (node) => {
    if (ts.isMethodDeclaration(node) || ts.isDecorator(node)) {
      return [newLine, node];
    }
    return ts.visitEachChild(node, visit, context);
  };
  return (node: any) => ts.visitNode(node, visit);
};
