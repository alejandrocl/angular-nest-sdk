import ts from "typescript";

import { SdkMethod, SdkTree } from "../common/sdk-tree";
import { WriterAdapter } from "../common/writer-adapter";
import { AddNewLines } from "./format-transformer";

const f = ts.factory;
export class AngularWriter implements WriterAdapter {
  write(tree: SdkTree): string {
    const printer = ts.createPrinter();

    let sourceFile = ts.createSourceFile(
      `output.ts`,
      "",
      ts.ScriptTarget.Latest,
      false,
      ts.ScriptKind.TS
    );

    let resultFile = f.updateSourceFile(sourceFile, [
      ...this.createImportDeclarations(tree),
      this.createClass(tree),
    ]);

    resultFile = ts.transform(resultFile, [AddNewLines]).transformed[0];

    return printer.printNode(ts.EmitHint.SourceFile, resultFile, sourceFile);
  }

  private createClass(tree: SdkTree) {
    return f.createClassDeclaration(
      /** modifiers */ [
        this.createInjectableDecorator(),
        f.createModifier(ts.SyntaxKind.ExportKeyword),
      ],
      tree.name,
      /** type parameters */ undefined,
      /** heritage */ undefined,
      /** members */ [
        this.createClassConstructor(),
        ...this.createMethods(tree),
      ]
    );
  }

  private createImportDeclarations(tree: SdkTree) {
    return [
      f.createImportDeclaration(
        undefined,
        f.createImportClause(
          false,
          undefined,
          f.createNamedImports([
            f.createImportSpecifier(
              false,
              undefined,
              f.createIdentifier("Injectable")
            ),
            f.createImportSpecifier(
              false,
              undefined,
              f.createIdentifier("Observable")
            ),
          ])
        ),
        f.createStringLiteral("@angular/core")
      ),
      f.createImportDeclaration(
        undefined,
        f.createImportClause(
          false,
          undefined,
          f.createNamedImports([
            f.createImportSpecifier(
              false,
              undefined,
              f.createIdentifier("HttpClient")
            ),
          ])
        ),
        f.createStringLiteral("@angular/common/http")
      ),
      ...tree.imports.map((importDecl) =>
        f.createImportDeclaration(
          undefined,
          f.createImportClause(
            false,
            undefined,
            f.createNamedImports([
              ...importDecl.classes.map((c) =>
                f.createImportSpecifier(false, undefined, f.createIdentifier(c))
              ),
            ])
          ),
          f.createStringLiteral(importDecl.path)
        )
      ),
    ];
  }

  private createInjectableDecorator() {
    return f.createDecorator(
      f.createCallExpression(f.createIdentifier("Injectable"), undefined, [
        f.createObjectLiteralExpression([
          f.createPropertyAssignment(
            "providedIn",
            f.createStringLiteral("root")
          ),
        ]),
      ])
    );
  }

  private createClassConstructor() {
    return f.createConstructorDeclaration(
      undefined,
      [
        f.createParameterDeclaration(
          f.createModifiersFromModifierFlags(ts.ModifierFlags.Private),
          undefined,
          "http",
          undefined,
          f.createTypeReferenceNode("HttpClient", undefined),
          undefined
        ),
      ],
      f.createBlock([], false)
    );
  }

  private createMethods(tree: SdkTree) {
    return tree.methods.map((method) => {
      const parameters = this.createParameters(method);
      const httpMethod = this.createHttpMethod(method);

      return f.createMethodDeclaration(
        undefined,
        undefined,
        method.functionName,
        undefined,
        undefined,
        parameters,
        f.createTypeReferenceNode("Observable", [
          f.createTypeReferenceNode(method?.returnType ?? "any", undefined),
        ]),
        f.createBlock(
          [
            f.createReturnStatement(
              f.createCallExpression(
                f.createPropertyAccessExpression(
                  f.createThis(),
                  f.createIdentifier("http." + httpMethod)
                ),
                [
                  f.createTypeReferenceNode(
                    method?.returnType ?? "any",
                    undefined
                  ),
                ],
                [
                  //TODO: Missing path for method
                  f.createStringLiteral(`${tree.path}${method.endpoint}`),
                  ...method.parameters.map((p) => f.createIdentifier(p.name)),
                ]
              )
            ),
          ],
          true
        )
      );
    });
  }

  private createParameters(method: SdkMethod) {
    return method.parameters.map((param) =>
      f.createParameterDeclaration(
        undefined,
        undefined,
        param.name,
        undefined,
        param.type
          ? f.createTypeReferenceNode(param.type)
          : f.createKeywordTypeNode(ts.SyntaxKind.AnyKeyword)
      )
    );
  }

  private createHttpMethod(method: SdkMethod) {
    return method.httpMethod.toLocaleLowerCase();
  }
}
