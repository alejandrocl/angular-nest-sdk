import fs from "fs/promises";
import ts from "typescript";

import { ReaderAdapter } from "./common/reader-adapter";
import { WriterAdapter } from "./common/writer-adapter";

export const sdk = async (
  inputFilePath: string,
  outputFilePath: string,
  reader: ReaderAdapter,
  writer: WriterAdapter,
  options?: { outputJson?: string }
) => {
  const program = ts.createProgram([inputFilePath], {
    target: ts.ScriptTarget.Latest,
    module: ts.ModuleKind.CommonJS,
  });

  const tree = reader.read(program, inputFilePath);
  const output = writer.write(tree);
  fs.writeFile(outputFilePath, output);

  if (options?.outputJson) {
    fs.writeFile(options.outputJson, JSON.stringify(tree, null, 2));
  }
};
