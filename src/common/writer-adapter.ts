import { SdkTree } from "./sdk-tree";

export interface WriterAdapter {
  write(tree: SdkTree): string;
}
