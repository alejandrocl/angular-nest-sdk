export type HttpMethod =
  | "GET"
  | "POST"
  | "PUT"
  | "DELETE"
  | "OPTIONS"
  | "PATCH";

export type SdkImport = {
  classes: string[];
  path: string;
};

export type SdkParameter = {
  name: string;
  type: string;
};

export type SdkMethod = {
  functionName: string;
  httpMethod: HttpMethod;
  endpoint: string;
  returnType: string;
  parameters: SdkParameter[];
};

export type SdkTree = {
  name: string;
  path: string;
  imports: SdkImport[];
  methods: SdkMethod[];
};
