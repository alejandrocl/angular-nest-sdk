import ts from "typescript";
import { SdkTree } from "./sdk-tree";

export interface ReaderAdapter {
  read(program: ts.Program, fileName: string): SdkTree;
}
